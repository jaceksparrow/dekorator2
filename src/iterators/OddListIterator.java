package iterators;

import java.util.ListIterator;
import java.util.NoSuchElementException;

public class OddListIterator implements ListIterator<Integer> {

    ListIterator<Integer> baseIterator;

    public OddListIterator(ListIterator<Integer> baseIterator) {
        this.baseIterator = baseIterator;
    }

    @Override
    public boolean hasNext() {
        boolean hasNext = this.baseIterator.hasNext();
        if (hasNext) {
            try {
                if (isOdd(next())) {
                    previous();
                    return true;
                } else {
                    return hasNext();
                }
            } catch (NoSuchElementException e) {
                return false;
            }
        } else {
            return false;
        }
    }

    @Override
    public Integer next() {
        int next = baseIterator.next();
        if (isOdd(next)) {
            return next;
        } else {
            return next();
        }
    }

    @Override
    public boolean hasPrevious() {
        return baseIterator.hasPrevious();
    }

    @Override
    public Integer previous() {
        return baseIterator.previous();
    }

    @Override
    public int nextIndex() {
        return baseIterator.nextIndex();
    }

    @Override
    public int previousIndex() {
        return baseIterator.previousIndex();
    }

    @Override
    public void remove() {
        baseIterator.remove();
    }

    @Override
    public void set(Integer integer) {

    }

    @Override
    public void add(Integer integer) {

    }

    private boolean isOdd(int value) {
        return value % 2 != 0;
    }
}
