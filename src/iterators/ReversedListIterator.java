package iterators;

import java.util.ListIterator;
import java.util.NoSuchElementException;

public class ReversedListIterator implements ListIterator<Integer> {

    private ListIterator<Integer> baseIterator;

    public ReversedListIterator(ListIterator<Integer> baseIterator) {
        this.baseIterator = baseIterator;
    }

    @Override
    public boolean hasNext() {
        return baseIterator.hasPrevious();
    }

    @Override
    public Integer next() {
        return baseIterator.previous();
    }

    @Override
    public boolean hasPrevious() {
        return baseIterator.hasPrevious();
    }

    @Override
    public Integer previous() {
        return baseIterator.previous();
    }

    @Override
    public int nextIndex() {
        return baseIterator.nextIndex();
    }

    @Override
    public int previousIndex() {
        return baseIterator.previousIndex();
    }

    @Override
    public void remove() {
        baseIterator.remove();
    }

    @Override
    public void set(Integer integer) {

    }

    @Override
    public void add(Integer integer) {

    }
}
