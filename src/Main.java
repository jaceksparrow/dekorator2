import decorators.EvenListDecorator;
import decorators.OddListDecorator;
import decorators.ReversedListDecorator;

import java.util.*;
import java.util.stream.Collectors;

public class Main {

    //main title
    private static final String PROGRAM_TITLE = "****************************************" +
            "\nProsty dekorator listy (ArrayList) liczb całkowitych" +
            "\nWciśnij odpowiedni przycisk..." +
            "\n****************************************";

    //available options titles
    private static final String SIMPLE_LIST_OPTION = "1. Nieudekorowana lista";
    private static final String EVEN_LIST_OPTION = "2. Lista liczb parzystych";
    private static final String ODD_LIST_OPTION = "3. Lista liczb nieparzystych";
    private static final String REVERSED_LIST_OPTION = "4. Odwrócona lista";
    private static final String QUIT_OPTION = "0. Zamknij program";

    //option type values
    private static final int SIMPLE_LIST_VALUE = 1;
    private static final int EVEN_LIST_VALUE = 2;
    private static final int ODD_LIST_VALUE = 3;
    private static final int REVERSED_LIST_VALUE = 4;
    private static final int QUIT_VALUE = 0;

    //other messages
    private static final String WRONG_INPUT_MESSAGE = "Podano niewłaściwą wartość";

    private static final int[] numbers = {4, 8, 15, 16, 23, 42};

    public static void main(String args[]) {
        Scanner scanner = new Scanner(System.in);

        while (true) {
            printTitles();
            try {
                int input = scanner.nextInt();
                if (input == QUIT_VALUE) {
                    break;
                } else {
                    handleInput(input);
                }
            } catch (InputMismatchException e) {
                System.out.println(WRONG_INPUT_MESSAGE);
                scanner.next();
            }
        }
        scanner.close();
    }

    /**
     * Prints "GUI" part
     */
    private static void printTitles() {
        System.out.println(PROGRAM_TITLE);
        System.out.println(SIMPLE_LIST_OPTION);
        System.out.println(EVEN_LIST_OPTION);
        System.out.println(ODD_LIST_OPTION);
        System.out.println(REVERSED_LIST_OPTION);
        System.out.println(QUIT_OPTION);
    }

    /**
     * Handles user's input
     *
     * @param input input number
     */
    @SuppressWarnings("unchecked")
    private static void handleInput(int input) {
        ArrayList<Integer> list = Arrays.stream(numbers).boxed().collect(Collectors.toCollection(ArrayList::new));
        switch (input) {
            case SIMPLE_LIST_VALUE:
                printSimpleList(list);
                break;
            case EVEN_LIST_VALUE:
                printEvenList(list);
                break;
            case ODD_LIST_VALUE:
                printOddList(list);
                break;
            case REVERSED_LIST_VALUE:
                printReversedList(list);
                break;
            default:
                System.out.println(WRONG_INPUT_MESSAGE);
                break;
        }
    }

    private static void printSimpleList(ArrayList<Integer> list) {
        System.out.println();
        System.out.println(list);
        System.out.println();
    }

    private static void printEvenList(ArrayList<Integer> list) {
        list = new EvenListDecorator(list);
        System.out.println();
        System.out.println(list);
        System.out.println();
    }

    private static void printOddList(ArrayList<Integer> list) {
        list = new OddListDecorator(list);
        System.out.println();
        System.out.println(list);
        System.out.println();
    }

    private static void printReversedList(ArrayList<Integer> list) {
        list = new ReversedListDecorator(list);
        System.out.println();
        System.out.println(list);
        System.out.println();
    }
}
